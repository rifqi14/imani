package com.example.rifqi.imaniprima;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.support.v7.app.ActionBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;



import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;


import java.io.UnsupportedEncodingException;


public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {


 
    private MqttAndroidClient client;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private String TAG = "MainActivity";
    private PahoMqttClient pahoMqttClient;
    private String clientid = "";
    private Timer myTimer;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;

    private TextView tvSubscribedMsg;
    private TextView tvConnectStatus;
    private EditText etBroker;
    private EditText etUName;
    private EditText etPWord;
    private EditText etSubTopic;
    private EditText etPubTopic;
    private EditText etPubMsg;


    private BottomNavigationView navigation;
 //   private LatLng location;
 private Location location;

    //Callback when bottom navigation item is selected
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {


        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            String msg_new = "";

           // BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
            //BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

            switch (item.getItemId()) {
                case R.id.navigation_connect:
                    //--- Set Connection Parameters ---
                    String urlBroker = etBroker.getText().toString().trim();
                    String username = etUName.getText().toString().trim();
                    String password = etPWord.getText().toString().trim();

                    Random r = new Random();        //Unique Client ID for connection
                    int i1 = r.nextInt(5000 - 1) + 1;
                    clientid = "mqtt" + i1;

                    if (pahoMqttClient.mqttAndroidClient.isConnected()) {
                        //Disconnect and Reconnect to  Broker
                        try {
                            //Disconnect from Broker
                            pahoMqttClient.disconnect(client);
                            //Connect to Broker
                            client = pahoMqttClient.getMqttClient(getApplicationContext(), urlBroker, clientid, username, password);
                            //Set Mqtt Message Callback
                            mqttCallback();
                        } catch (MqttException e) {
                        }
                    } else {
                        //Connect to Broker
                        client = pahoMqttClient.getMqttClient(getApplicationContext(), urlBroker, clientid, username, password);
                        //Set Mqtt Message Callback
                        mqttCallback();
                    }
                    return true;
                case R.id.navigation_subscribe:
                    if (!pahoMqttClient.mqttAndroidClient.isConnected()) {
                        msg_new = "Currently not connected to MQTT broker: Must be connected to subscribe to a topic\r\n";
                        tvSubscribedMsg.setText("");
                        tvSubscribedMsg.setText(msg_new);
                        return true;
                    }
                    String topic = etSubTopic.getText().toString().trim();
                    if (!topic.isEmpty()) {
                        try {
                            pahoMqttClient.subscribe(client, topic, 1);
                            msg_new = "Added subscription topic: " + etSubTopic.getText() + "\r\n";
                            tvSubscribedMsg.setText("");
                            tvSubscribedMsg.setText(msg_new);

                        } catch (MqttException e) {
                            e.printStackTrace();
                        }
                    }
                    return true;
                case R.id.navigation_publish:
                    //Check if connected to broker
                    if (!pahoMqttClient.mqttAndroidClient.isConnected()) {
                        //msg_new = "Currently not connected to MQTT broker: Must be connected to publish message to a topic\r\n";
                        tvSubscribedMsg.setText("");
                        tvSubscribedMsg.setText("Currently not connected to MQTT broker: Must be connected to publish message to a topic");

                        return true;
                    }
                    //Publish non-blank message
                    String pubtopic = etPubTopic.getText().toString().trim();
                    String msg = etPubMsg.getText().toString().trim();
                    if (!msg.isEmpty()) {
                        try {
                            pahoMqttClient.publishMessage(client, msg, 1, pubtopic);
                            msg_new = "Message sent to pub topic: " + etPubTopic.getText() + "\r\n";
                            tvSubscribedMsg.append(msg_new);
                        } catch (MqttException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    return true;
                case R.id.navigation_clear:
                    //Clear message field
                    tvSubscribedMsg.setText("");
                    return true;
                case R.id.navigation_exit:
                    System.exit(0);
                    return true;
            }
            return false;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);                                                 // Main Activity layout file

        ActionBar actionBar = getSupportActionBar();                                            // Add Icon to title bar
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.ic_launcher_round);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        tvSubscribedMsg = (TextView) findViewById(R.id.tvSubscribedMsg);
        tvConnectStatus = (TextView) findViewById(R.id.tvConnectStatus);
        etBroker = (EditText) findViewById(R.id.etBroker);
        etUName = (EditText) findViewById(R.id.etUName);
        etPWord = (EditText) findViewById(R.id.etPWord);
        etSubTopic = (EditText) findViewById(R.id.etSubTopic);
        etPubTopic = (EditText) findViewById(R.id.etPubTopic);
        etPubMsg = (EditText) findViewById(R.id.etPubMsg);


        //Generate unique client id for MQTT broker connection
        Random r = new Random();
        int i1 = r.nextInt(5000 - 1) + 1;
        clientid = "mqtt" + i1;

        //Get Edit field values from layout GUI

        String urlBroker = etBroker.getText().toString().trim();
        String username = etUName.getText().toString().trim();
        String password = etPWord.getText().toString().trim();

        pahoMqttClient = new PahoMqttClient();
        client = pahoMqttClient.getMqttClient(getApplicationContext(),                        // Connect to MQTT Broker
                urlBroker,
                clientid,
                username,
                password
        );
        //Register Bottom Navigation Callback
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);      // Set nav menu "Select" callback
        BottomNavigationViewHelper.disableShiftMode(navigation);                                // Make all Text Visible

        //Create listener for MQTT messages.
        mqttCallback();

        //Create Timer to report MQTT connection status every 1 second
        myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                ScheduleTasks();
            }

        }, 0, 1000);
    }

    private void ScheduleTasks() {
        //This method is called directly by the timer
        //and runs in the same thread as the timer.

        //We call the method that will work with the UI
        //through the runOnUiThread method.
        this.runOnUiThread(RunScheduledTasks);
    }


    private Runnable RunScheduledTasks = new Runnable() {
        public void run() {
            //This method runs in the same thread as the UI.

            //Check MQTT Connection Status

            String msg_new = "";

            if (pahoMqttClient.mqttAndroidClient.isConnected()) {
                msg_new = "Connected\r\n";
                tvConnectStatus.setTextColor(0xFF00FF00); //Green if connected
                tvConnectStatus.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            } else {
                msg_new = "Disconnected\r\n";
                tvConnectStatus.setTextColor(0xFFFF0000); //Red if not connected
                tvConnectStatus.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            }
            tvConnectStatus.setText(msg_new);
        }
    };


    // Called when a subscribed message is received
    protected void mqttCallback() {
        client.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) {
                tvConnectStatus.setText("Connection Lost");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {

                if (topic.equals("mycustomtopic1")) {
                    //Add custom message handling here (if topic = "mycustomtopic1")
                } else if (topic.equals("mycustomtopic2")) {
                    //Add custom message handling here (if topic = "mycustomtopic2")
                } else {
                    String msg = "topic: " + topic + "\r\nMessage: " + message.toString() + "\r\n";
                    tvSubscribedMsg.append(msg);
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });
    }
//===========================================MAPS====================================================
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMyLocationButtonClickListener(onMyLocationButtonClickListener);
        mMap.setOnMyLocationClickListener(onMyLocationClickListener);
        enableMyLocationIfPermitted();
        mMap.getUiSettings().setZoomControlsEnabled(true);
        showDefaultLocation();
        mMap.setMinZoomPreference(5);

    }


    private void enableMyLocationIfPermitted() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        } else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
        }
    }



    public void  showDefaultLocation () {
getMyLocation();

    }
    public void getMyLocation()
    {

        final boolean[] isMyLocationSet = {false};
        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

            @Override
            public void onMyLocationChange(Location location) {
                if(isMyLocationSet[0])
                    return;
                isMyLocationSet[0] = true;
                // TODO Auto-generated method stub
                Log.v("current location lat:", ""+location.getLatitude());
                Log.v("current location lng:", ""+location.getLongitude());
                mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
                CameraUpdate center=
                        CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()),16);
                CameraUpdate zoom=CameraUpdateFactory.zoomTo(12);

                mMap.moveCamera(center);
                mMap.animateCamera(zoom);
            }
        });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enableMyLocationIfPermitted();
                } else {
                    showDefaultLocation();
                }
                return;
            }
        }
    }



    private GoogleMap.OnMyLocationButtonClickListener onMyLocationButtonClickListener =
            new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    mMap.setMinZoomPreference(15);
                    return false;
                }
            };

    public GoogleMap.OnMyLocationClickListener onMyLocationClickListener =
            new GoogleMap.OnMyLocationClickListener() {
                @Override
                public void onMyLocationClick(@NonNull Location location) {

                    mMap.setMinZoomPreference(12);
                    CircleOptions circleOptions = new CircleOptions();
                    LatLng latLng = new LatLng(location.getLatitude(),
                            location.getLongitude());

                    String str = "Long: " + String.valueOf(location.getLongitude()) +
                            ", Lat: " + String.valueOf(location.getLatitude());
                    Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                    etPubMsg.setText(str);

                    //Publish non-blank message
                    String pubtopic = etPubTopic.getText().toString().trim();
                    if (!pahoMqttClient.mqttAndroidClient.isConnected()) {
                        //msg_new = "Currently not connected to MQTT broker: Must be connected to publish message to a topic\r\n";
                        tvSubscribedMsg.setText("");
                        tvSubscribedMsg.setText("Currently not connected to MQTT broker: Must be connected to publish message to a topic");
                    } else {

                        try {
                            pahoMqttClient.publishMessage(client, str, 1, pubtopic);
                            tvSubscribedMsg.setText(str);
                        } catch (MqttException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }


                }
            };
}
